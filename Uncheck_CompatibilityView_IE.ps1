#-----------------------------------------------------#
# Changes Compatibility view off for Intranet sites 
# in IE setting
#-----------------------------------------------------#

Write-Output 'Creating HKU drive...'
New-PSDrive -Name HKU -PSProvider Registry -Root Registry::HKEY_USERS -ErrorAction SilentlyContinue | Out-Null
 
Write-Output 'Parsing User SIDs...'
$UserSids = (Get-ChildItem HKU: | Where-Object { $_.Name -match 'S-\d-\d+-(\d+-){1,14}\d+$' }).PSChildName
 
foreach ($SID in $UserSids) {
    Write-Output "Updating SID: $($SID)"
    if (test-path "HKU:\$SID\Software\Microsoft\Internet Explorer\BrowserEmulation") {
        Write-Output 'IntranetCompatibilityMode found!'
        Set-ItemProperty -Path "HKU:\$SID\Software\Microsoft\Internet Explorer\BrowserEmulation" -Name IntranetCompatibilityMode -Value 0 -Force | Out-Null
        Write-Output 'Compatibility View for Intranet Sites is now disabled'
    } else {
        Write-Output 'IntranetCompatibilityMode registry item not found!'
    }
}
# Now delete the task after first run
Unregister-ScheduledTask -TaskName "Uncheck Compatibility View IE" -Confirm:$false
# End notepad.exe process if it opens
Stop-Process -Name "notepad" -Force